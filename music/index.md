---
layout: default
title: Music
permalink: /music/
---

## Transcriptions

### Ahwak (Abd al-Halim Hafez) 

- [C instruments](./ahwak/ahwak-c.pdf)
- [B flat instruments](./ahwak/ahwak-b-flat.pdf)
- [E flat instruments](./ahwak/ahwak-e-flat.pdf)
- [F instruments](./ahwak/ahwak-f.pdf)
- [Lyrics and translation](./ahwak/ahwak-text.pdf)
- [YouTube link](https://www.youtube.com/watch?v=kPFOEld732k)

### Alger Alger (Line Monty)

- [C instruments](./alger-alger/alger-alger-c.pdf)
- [Bass clef instruments](./alger-alger/alger-alger-bass.pdf)
- [B flat instruments](./alger-alger/alger-alger-b-flat.pdf)
- [E flat instruments](./alger-alger/alger-alger-e-flat.pdf)
- [F instruments](./alger-alger/alger-alger-f.pdf)
- [YouTube link](https://www.youtube.com/watch?v=-sT9Z6auBbg)

### Al-Hilwa Di (Darwish)

- [C instruments](./al-hilwa-di/al-hilwa-di-c.pdf)
- [Bass clef instruments](./al-hilwa-di/al-hilwa-di-bass.pdf)
- [B flat instruments](./al-hilwa-di/al-hilwa-di-b-flat.pdf)
- [E flat instruments](./al-hilwa-di/al-hilwa-di-e-flat.pdf)
- [F instruments](./al-hilwa-di/al-hilwa-di-f.pdf)
- [Transliteration](./al-hilwa-di/al-hilwa-di-transliteration.pdf)
- [YouTube link](https://www.youtube.com/watch?v=41r2BzAuvzo)

### Al-Hinna (unknown)

- [C instruments](./al-hinna/al-hinna-c.pdf)
- [B flat instruments](./al-hinna/al-hinna-b-flat.pdf)
- [E flat instruments](./al-hinna/al-hinna-e-flat.pdf)
- [F instruments](./al-hinna/al-hinna-f.pdf)
- [Bandcamp link](https://canary-records.bandcamp.com/track/alhinna)

### ʿA Nadda (Sabah)

- [C instruments](./an-nada/an-nada-c.pdf)
- [Bass clef instruments](./an-nada/an-nada-bass.pdf)
- [B flat instruments](./an-nada/an-nada-b-flat.pdf)
- [E flat instruments](./an-nada/an-nada-e-flat.pdf)
- [F instruments](./an-nada/an-nada-f.pdf)
- [Translation (from shira.net)](http://www.shira.net/music/lyrics/a-nada.htm)
- [YouTube link](https://www.youtube.com/watch?v=VEXC_getOlY)

### Ashira/Mi Chamocha

- [C instruments](./ashira-mi-chamocha/ashira-mi-chamocha-c.pdf)
- [B flat instruments](./ashira-mi-chamocha/ashira-mi-chamocha-b-flat.pdf)
- [E flat instruments](./ashira-mi-chamocha/ashira-mi-chamocha-e-flat.pdf)
- [F instruments](./ashira-mi-chamocha/ashira-mi-chamocha-f.pdf)

### Bay A Glazele Mashke

- [C instruments](./bay-a-glezele-mashke/bay-a-glezele-mashke-c.pdf)
- [B flat instruments](./bay-a-glezele-mashke/bay-a-glezele-mashke-b-flat.pdf)
- [E flat instruments](./bay-a-glezele-mashke/bay-a-glezele-mashke-e-flat.pdf)
- [F instruments](./bay-a-glezele-mashke/bay-a-glezele-mashke-f.pdf)
- [YouTube link](https://youtu.be/0jUFWhwDt6s)

### Gana al-Hawa (Baligh Hamdi)

- [Lyrics](./gana-al-hawa/gana-al-hawa-lyrics.pdf)
- [C instruments](./gana-al-hawa/gana-al-hawa-c.pdf)
- [Bass clef instruments](./gana-al-hawa/gana-al-hawa-bass.pdf)
- [B flat instruments](./gana-al-hawa/gana-al-hawa-b-flat.pdf)
- [E flat instruments](./gana-al-hawa/gana-al-hawa-e-flat.pdf)
- [F instruments](./gana-al-hawa/gana-al-hawa-f.pdf)
- [Translation (from shira.net)](http://www.shira.net/music/lyrics/gana-el-hawa.htm)
- [YouTube link](https://www.youtube.com/watch?v=-RHgvrG69iU)

### Hadra from the Moroccan Hamadsha tradition

As played by Abd al-Rahim Amrani's ensemble from Fez

- [C instruments](./hadra/hadra-c.pdf)
- [B flat instruments](./hadra/hadra-b-flat.pdf)
- [E flat instruments](./hadra/hadra-e-flat.pdf)
- [F instruments](./hadra/hadra-f.pdf)
- [Audio recording](./hadra/hamadsha-hadra-recording.mp3)

### Hakam Alayna al-Hawa, muqaddima (Baligh Hamdi)

- [Manuscript (in C)](./hakam-alayna-al-hawa/hakam-alayna-al-hawa-manuscript.pdf)
- [C instruments](./hakam-alayna-al-hawa/hakam-alayna-al-hawa-c.pdf)
- [Bass clef instruments](./hakam-alayna-al-hawa/hakam-alayna-al-hawa-bass.pdf)
- [B flat instruments](./hakam-alayna-al-hawa/hakam-alayna-al-hawa-b-flat.pdf)
- [E flat instruments](./hakam-alayna-al-hawa/hakam-alayna-al-hawa-e-flat.pdf)
- [F instruments](./hakam-alayna-al-hawa/hakam-alayna-al-hawa-f.pdf)
- [YouTube link](https://www.youtube.com/watch?v=8MOBQoXl6Sk)

### Hawwil ya Ghannam (Amer Khadaj)

- [Complete](./hawwil-ya-ghannam/hawwil-ya-ghannam-complete.pdf)
- [Lyrics](./hawwil-ya-ghannam/hawwil-ya-ghannam-lyrics.pdf)
- [C instruments](./hawwil-ya-ghannam/hawwil-ya-ghannam-c.pdf)
- [Bass clef instruments](./hawwil-ya-ghannam/hawwil-ya-ghannam-bass.pdf)
- [B flat instruments](./hawwil-ya-ghannam/hawwil-ya-ghannam-b-flat.pdf)
- [E flat instruments](./hawwil-ya-ghannam/hawwil-ya-ghannam-e-flat.pdf)
- [F instruments](./hawwil-ya-ghannam/hawwil-ya-ghannam-f.pdf)

### Miserlou (Eddie Kochak)

- [Complete](./miserlou/miserlou-complete.pdf)
- [Lyrics](./miserlou/miserlou-lyrics.pdf)
- [C instruments](./miserlou/miserlou-complete-c.pdf)
- [Bass clef instruments](./miserlou/miserlou-bass.pdf)
- [B flat instruments](./miserlou/miserlou-b-flat.pdf)
- [E flat instruments](./miserlou/miserlou-e-flat.pdf)
- [F instruments](./miserlou/miserlou-f.pdf)
- [YouTube link](https://www.youtube.com/watch?v=RY3gNtETDl8)

### Le Swing des Petits Taxis (Sami Maghrebi)

- [Lyrics](./petits-taxis/le-swing-des-petits-taxis-text.pdf)
- [C instruments (with chord changes)](./petits-taxis/le-swing-des-petits-taxis.pdf)
- [Transposed down to B flat (with chord changes)](./petits-taxis/le-swing-des-petits-taxis-in-b-flat.pdf)
- [Bass clef instruments](./petits-taxis/le-swing-des-petits-taxis-bass.pdf)
- [B flat instruments](./petits-taxis/le-swing-des-petits-taxis-b-flat.pdf)
- [E flat instruments](./petits-taxis/le-swing-des-petits-taxis-e-flat.pdf)
- [F instruments](./petits-taxis/le-swing-des-petits-taxis-f.pdf)

### Qaddam Maya (Andalusi)

- [Complete](./quddam-maya/quddam_maya.pdf)
- [YouTube link](https://www.youtube.com/watch?v=Zq_jYMB3tSI)

### Raks Araby (George Abdo)

- [Complete](./raks-araby/raks-araby-complete.pdf)
- [C instruments](./raks-araby/raks-araby-c.pdf)
- [Bass clef instruments](./raks-araby/raks-araby-bass.pdf)
- [B flat instruments](./raks-araby/raks-araby-b-flat.pdf)
- [E flat instruments](./raks-araby/raks-araby-e-flat.pdf)
- [F instruments](./raks-araby/raks-araby-f.pdf)
- [Spotify link](https://open.spotify.com/track/4jNvfzdlllktR5JvkVfLgi)

### Samaʿi Sehir al-Sharq

- [C instruments](./samai-sehir-al-sharq/samai-sehir-al-sharq.pdf)
- [Bass clef instruments](./samai-sehir-al-sharq/samai-sehir-al-sharq-bass.pdf)
- [B flat instruments](./samai-sehir-al-sharq/samai-sehir-al-sharq-b-flat.pdf)
- [E flat instruments](./samai-sehir-al-sharq/samai-sehir-al-sharq-e-flat.pdf)
- [F instruments](./samai-sehir-al-sharq/samai-sehir-al-sharq-f.pdf)

### Ta Marva Matia Sou (George Abdo)

- [Complete](./ta-marva-matia-sou/ta-mavra-matia-sou-complete.pdf)
- [Lyrics](./ta-marva-matia-sou/ta-mavra-matia-sou-lyrics.pdf)
- [C instruments](./ta-marva-matia-sou/ta-marva-matia-sou-c.pdf)
- [Bass clef instruments](./ta-marva-matia-sou/ta-marva-matia-sou-bass.pdf)
- [B flat instruments](./ta-marva-matia-sou/ta-marva-matia-sou-b-flat.pdf)
- [E flat instruments](./ta-marva-matia-sou/ta-marva-matia-sou-e-flat.pdf)
- [F instruments](./ta-marva-matia-sou/ta-marva-matia-sou-f.pdf)
- [YouTube link](https://www.youtube.com/watch?v=DsXuRofO8qg)

### Ya Samira, Ya Samira (Hanan and Fairuz)

- [Complete](./ya-samira-ya-samira/ya-samira-ya-samira-complete.pdf)
- [Lyrics](./ya-samira-ya-samira/ya-samira-ya-samira-lyrics.pdf)
- [C instruments](./ya-samira-ya-samira/ya-samira-ya-samira-c.pdf)
- [Bass clef instruments](./ya-samira-ya-samira/ya-samira-ya-samira-bass.pdf)
- [B flat instruments](./ya-samira-ya-samira/ya-samira-ya-samira-b-flat.pdf)
- [E flat instruments](./ya-samira-ya-samira/ya-samira-ya-samira-e-flat.pdf)
- [F instruments](./ya-samira-ya-samira/ya-samira-ya-samira-f.pdf)
- [YouTube link](https://www.youtube.com/watch?v=Mp6Uivy5ed0)

### Zina (Abd al-Wahab)

- [C instruments](./zina/zina-c.pdf)
- [Bass clef instruments](./zina/zina-bass.pdf)
- [B flat instruments](./zina/zina-b-flat.pdf)
- [E flat instruments](./zina/zina-e-flat.pdf)
- [F instruments](./zina/zina-f.pdf)
- [YouTube link](https://www.youtube.com/watch?v=ouCtyxwhPRk)

### Moroccan rhythms

This includes some sample rhythms from the ʿissawa and hamadsha traditions.

- [Moroccan rhythms](./morocco-rhythms/morocco-rhythms.pdf)

### Nay exercises

This introduces some _qafla_ examples in different _ajnas_. It is derived from a short demonstration by Scott Marcus.

- [Ajnas and maqam qafla examples with transpositions](./ajnas/ajnas.pdf)
- [Version with nay fingerings](./ajnas/nay.pdf)
